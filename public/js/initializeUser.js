    // create userProto
    const userProto = {
        get: function (property) {
            return this.property;
        },
        set: function (value) {
            this.value = value;
        },
        statusHashMap: {
            personalInfo,
            address,
            bankDetails,
        },
        status: {},
        setStatus: function(status) {
            this.status = this.statusHashMap[status];
            this.status.show();
            this.status.manageNextButton();
            localStorage.setItem("currentStatus", status);
        },
        next: function() {
            this.status.next(this);
            this.status.manageNextButton();
        },
        previous: function() {
            this.status.previous(this);
            this.status.manageNextButton();
        },
        manageNextButton: function() {
            this.status.manageNextButton();
        }
    };

    // create user
    const user = {
        name: "",
        surname: "",
        phoneNumber: "",
        street: "",
        houseNumber: "",
        zipCode: "",
        city: "",
        bankAccountOwner: "",
        iban: "",
    };

    Object.setPrototypeOf(user, userProto);

    // retrieve state from local storage
    const userData = JSON.parse(localStorage.getItem("user"));

    if (userData) {
        // map local storage values on user
        for (const prop in user) {
            if (user.hasOwnProperty(prop)) {
                user[prop] = userData[prop];
            }
        }
    }
