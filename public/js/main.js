(() => {
    // DOM vars
    const personalInfoDOM = $("#personal-info");
    const addressDOM = $("#address");
    const bankDetailsDOM = $("#bank-details");
    const next = $(".next");
    const prev = $(".previous");

    // get status
    const globalStatus = localStorage.getItem("globalStatus");

    // hide all elements
    personalInfoDOM.hide();
    addressDOM.hide();
    bankDetailsDOM.hide();

    // populate input fields with values from user fields
    $(".active").each(function (i, el) {
        $(this).val(user[$(el).data("field")]);
    });

    $(".active").focusout(function (e) {
        user.manageNextButton();
        user[$(e.target).data("field")] = $(e.target).val();
        localStorage.setItem("user", JSON.stringify(user));
    });

    next.click(() => {
        user.next();
    });
    prev.click(() => {
        user.previous();
    });

    // initial status
    if (!globalStatus) {
        userProto.setStatus("personalInfo");
        //userProto.status.show();

        localStorage.setItem("currentStatus", "personalInfo");
        localStorage.setItem("globalStatus", "personalInfo");
        localStorage.setItem("user", JSON.stringify(user));
    } else {
        userProto.setStatus(globalStatus);
        //userProto.status.show();

        // populate input fields with values from user fields
        $(".active").each(function (i, el) {
            $(this).val(user[$(el).data("field")]);
        });
    }
    userProto.manageNextButton();
})();
