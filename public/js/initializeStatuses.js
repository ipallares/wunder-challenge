    const statusObject = {
        status: "",
        next: function () {},
        previous: function () {},
        show: function () {},
    };

    // create personalInfo status
    const personalInfo = Object.create(statusObject);
    const address = Object.create(statusObject);
    const bankDetails = Object.create(statusObject);

    personalInfo.status = "personalInfo";
    personalInfo.previous = function (parent) {
        return null;
    };
    personalInfo.next = function (parent) {
        $("#personal-info").hide();
        $("#address").show();
        parent.setStatus('address');
        if (localStorage.getItem("globalStatus") != 'bankDetails') {
            localStorage.setItem("globalStatus", 'address');
        }
    };
    personalInfo.show = function () {
        $(".tab").hide();
        $("#personal-info").show();
    };
    personalInfo.manageNextButton = function() {
        $("#personal-info").show();
        if ($("#name").val() != '' && $("#surname").val() != '' &&  $("#phone-number").val() != '') {
            $("#personal-info .next").prop("disabled", false);
        } else {
            $("#personal-info .next").prop("disabled", true);
        }
    }

    // create address status
    address.status = "address";
    address.previous = function (parent) {
        $("#address").hide();
        $("#personal-info").show();
        parent.setStatus('personalInfo');
    };
    address.next = function (parent) {
        $("#address").hide();
        $("#bank-details").show();
        parent.setStatus('bankDetails');
        localStorage.setItem("globalStatus", 'bankDetails');
    };
    address.show = function () {
        $(".tab").hide();
        $("#address").show();
    };
    address.manageNextButton = function() {
        $("#address").show();
        if ($("#street").val() != '' && $("#house-number").val() != '' &&  $("#zip-code").val() != '' &&  $("#city").val() != '') {
            $("#address .next").prop("disabled", false);
        } else {
            $("#address .next").prop("disabled", true);
        }
    };

    // create bankDetails status
    bankDetails.status = "bankDetails";
    bankDetails.previous = function (parent) {
        $("#bank-details").hide();
        $("#address").show();
        parent.setStatus('address');
    };
    bankDetails.next = function (parent) {
        // IPT: This url should be dynamically loaded
        jQuery.post(
            'https://127.0.0.1:8000/user',
            localStorage.user,
            function(data) {
                data = JSON.parse(data);
                alert('The registration process was completed successfully. Your payment data id is "'+data.paymentDataId+'"');
                // populate input fields with values from user fields
                $(".active").each(function (i, el) {
                    $(this).val('');
                });
                parent.setStatus('personalInfo');
                localStorage.clear();
            })
            .fail(function(data) {
                alert("Sorry the process couldn't be finished, please contact the administrator (Error Code: "+data.status+").");
            });

    };
    bankDetails.show = function () {
        $(".tab").hide();
        $("#bank-details").show();
    };
    bankDetails.manageNextButton = function() {
        $("#bank-details").show();
        if ($("#bank-account-owner").val() != '' && $("#iban").val() != '') {
            $("#bank-details .next").prop("disabled", false);
        } else {
            $("#bank-details .next").prop("disabled", true);
        }
    }
