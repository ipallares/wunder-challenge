<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210320173504 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Creates the "user" table matching the User entity';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE user (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, 
                name VARCHAR(100) NOT NULL, 
                surname VARCHAR(120) NOT NULL, 
                phone_number VARCHAR(15) NOT NULL, 
                street VARCHAR(100) NOT NULL, 
                house_number VARCHAR(10) NOT NULL, 
                zip_code VARCHAR(10) NOT NULL, 
                city VARCHAR(100) NOT NULL, 
                bank_account_owner VARCHAR(255) NOT NULL, 
                iban VARCHAR(34) NOT NULL, 
                payment_data_id VARCHAR(255) DEFAULT NULL)'
        );
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE user');
    }
}
