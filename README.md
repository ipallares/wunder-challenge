# Installation

1.- Install Composer https://getcomposer.org/download/

2.- Install Symfony CLI https://symfony.com/download

3.- `composer install`

4.- `php bin/console doctrine:database:create`

5.- `php bin/console doctrine:migrations:migrate`

6.- `php bin/console doctrine:database:create -e test`

7.- `php bin/console doctrine:migrations:migrate -e test`

8.- `dos2unix bin/phpunit` # I just got an error when running phpunit because of wrong break line format. This command solved it.

9.- `./bin/phpunit`

10.- `symfony server:start`

11.- In Browser go to url : `https://127.0.0.1:8000`

# Comments to the code

##Improvements

* The frontend should be reorganised for clean code purposes. Some improvements that should be done in there:

    * Use Symfony Asset Component to properly handle the minimization of the files.
    
    * Build a proper class and dependencies structure.
    
    * Testing.
    
* The backend could be organised in a clean architecture mode:

    * Separating infrastructure, application and domain.
    
    * Using interfaces to decouple our business logic from the infrastructure (IoC). Using the Strategy Pattern this would allow us to easily replace implementations (database, payment service, json schema validator...) with no lateral effects.  
    
    * Use a queue system to decouple use cases from tasks related to them and be more flexible to external errors (payment server down for example). This would help us keep the Open Closed Principle as when adding tasks related to creating a new user (notifications, datasync...) we wouldn't need to touch the UserCreator service but add new consumers to the queue.   
    

## Principles applied

I have tried to put in practice some good principles, such as:

* Single Responsibility Purpose (backend). Every service has a clear goal which makes the code more modular, testable and maintainable.

* State Pattern (frontend). To keep track of the current status, tasks to be done in each case and possible transitions.

* Law of Demeter, hiding the internal structure of classes by exposing methods that encapsulate them (for example the User in frontend uses a hash to get the Status object on every stage but the client using the User class doesn't see it).

   

