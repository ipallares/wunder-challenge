<?php

declare(strict_types=1);

namespace App\Tests\Logic\UseCase;

use App\Logic\Converter\UserConverter;
use App\Logic\PaymentRequester;
use App\Logic\UseCase\UserCreator;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class UserCreatorTest extends KernelTestCase
{
    private UserCreator $userCreator;

    public function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $paymentRequester = new PaymentRequester(self::$container->getParameter('payment_endpoint'), $this->getMockHttpClient());
        $this->userCreator = new UserCreator(
            $paymentRequester,
            self::$container->get(UserConverter::class),
            self::$container->get(UserRepository::class)
        );
    }

    public function testUserCreation(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';

        $userEntity = $this->userCreator->create($userJson);

        $userJsonObject = json_decode($userJson);

        $this->assertEquals($userJsonObject->name, $userEntity->getName());
        $this->assertEquals($userJsonObject->surname, $userEntity->getSurname());
        $this->assertEquals($userJsonObject->phoneNumber, $userEntity->getPhoneNumber());
        $this->assertEquals($userJsonObject->street, $userEntity->getStreet());
        $this->assertEquals($userJsonObject->houseNumber, $userEntity->getHouseNumber());
        $this->assertEquals($userJsonObject->zipCode, $userEntity->getZipCode());
        $this->assertEquals($userJsonObject->city, $userEntity->getCity());
        $this->assertEquals($userJsonObject->bankAccountOwner, $userEntity->getBankAccountOwner());
        $this->assertEquals($userJsonObject->iban, $userEntity->getIban());
        $this->assertEquals(
            '258a0b1ed8e6aeeec8ce589b09f208da88200abccebba960db81a42531887fae1c340992fb49bc6b9f299900cf8cc886',
            $userEntity->getPaymentDataId()
        );

    }

    private function getMockHttpClient(): MockHttpClient
    {
        $responseBody = '{"paymentDataId":"258a0b1ed8e6aeeec8ce589b09f208da88200abccebba960db81a42531887fae1c340992fb49bc6b9f299900cf8cc886"}';

        return new MockHttpClient(
            new MockResponse([$responseBody])
        );
    }
}
