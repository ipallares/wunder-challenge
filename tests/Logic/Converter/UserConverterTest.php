<?php

declare(strict_types=1);

namespace App\Tests\Logic\Converter;

use App\Logic\Converter\UserConverter;
use JsonSchema\Exception\InvalidSchemaException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserConverterTest extends KernelTestCase
{
    private UserConverter $userConverter;

    public function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $this->userConverter = self::$container->get(UserConverter::class);
    }

    public function testConverter(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';
        $userEntity = $this->userConverter->convert($userJson);

        $userJsonObject = json_decode($userJson);
        $this->assertEquals($userJsonObject->name, $userEntity->getName());
        $this->assertEquals($userJsonObject->surname, $userEntity->getSurname());
        $this->assertEquals($userJsonObject->phoneNumber, $userEntity->getPhoneNumber());
        $this->assertEquals($userJsonObject->street, $userEntity->getStreet());
        $this->assertEquals($userJsonObject->houseNumber, $userEntity->getHouseNumber());
        $this->assertEquals($userJsonObject->zipCode, $userEntity->getZipCode());
        $this->assertEquals($userJsonObject->city, $userEntity->getCity());
        $this->assertEquals($userJsonObject->bankAccountOwner, $userEntity->getBankAccountOwner());
        $this->assertEquals($userJsonObject->iban, $userEntity->getIban());
    }

    public function testConverter_missingName(): void
    {
        $userJson = '{
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';

        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_missingSurname(): void
    {
        $userJson = '{
                        "name": "Max",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';

        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_missingPhoneNumber(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';

        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_missingStreet(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';

        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_missingHouseNumber(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';

        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_missingZipCode(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';

        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_missingCity(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';

        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_missingBankAccountOwner(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "iban": "DE89-1234-124-123-123"
                    }';

        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_missingIban(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann"
                    }';

        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_wrongNameFormat(): void
    {
        $userJson = '{
                        "name": 111,
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';
        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_wrongSurameFormat(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": 111,
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';
        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_wrongPhoneNumberFormat(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": 555555555,
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';
        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_wrongStreetFormat(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": 111,
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';
        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_wrongHouseNumberFormat(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": 123,
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';
        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_wrongZipCodeFormat(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": 12345,
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';
        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_wrongCityFormat(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": 111,
                        "bankAccountOwner": "Max Mustermann",
                        "iban": "DE89-1234-124-123-123"
                    }';
        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_wrongBankAccountOwnerFormat(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": 111,
                        "iban": "DE89-1234-124-123-123"
                    }';
        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }

    public function testConverter_wrongIbanFormat(): void
    {
        $userJson = '{
                        "name": "Max",
                        "surname": "Mustermann",
                        "phoneNumber": "555-555-555",
                        "street": "Musterstrasse",
                        "houseNumber": "123",
                        "zipCode": "12345X",
                        "city": "Berlin",
                        "bankAccountOwner": "Max Mustermann",
                        "iban": 111
                    }';
        $this->expectException(InvalidSchemaException::class);
        $this->userConverter->convert($userJson);
    }
}
