<?php

declare(strict_types=1);

namespace App\Logic\Validator;

use JsonSchema\Exception\InvalidSchemaException;

class UserValidator
{
    private JsonSchemaValidator $jsonSchemaValidator;

    private string $jsonSchemaPath;

    public function __construct(
        JsonSchemaValidator $jsonSchemaValidator,
        string $jsonSchemaPath
    ) {
        $this->jsonSchemaValidator = $jsonSchemaValidator;
        $this->jsonSchemaPath = $jsonSchemaPath;
    }

    /**
     * @param string $user
     *
     * @throws InvalidSchemaException
     */
    public function validate(string $user): void
    {
        // Other validations (valid values, not repeating unique fields...) would also take place here
        $this->jsonSchemaValidator->validate($user, $this->jsonSchemaPath);
    }
}
