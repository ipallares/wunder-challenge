<?php

declare(strict_types=1);

namespace App\Logic;

use App\Entity\User;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class PaymentRequester
{
    private string $paymentEndpoint;
    private HttpClientInterface $client;

    public function __construct(string $paymentEndpoint, HttpClientInterface $client)
    {
        $this->paymentEndpoint = $paymentEndpoint;
        $this->client = $client;
    }

    /**
     * @param User $user
     *
     * @return ResponseInterface
     *
     * @throws TransportExceptionInterface
     */
    public function request(User $user): ResponseInterface
    {
        return $this->client->request('POST', $this->paymentEndpoint, $this->getUserPaymentJson($user));
    }

    private function getUserPaymentJson(User $user): array
    {
        return [
          'json' => [
              'customerId' => $user->getId(),
              "iban" => $user->getIban(),
              "owner" => $user->getBankAccountOwner()
          ]
        ];
    }
}
