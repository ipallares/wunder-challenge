<?php

declare(strict_types=1);

namespace App\Logic\Converter;

use App\Entity\User;
use App\Logic\Validator\UserValidator;
use JsonSchema\Exception\InvalidSchemaException;

class UserConverter
{
    private UserValidator $userValidator;

    public function __construct(UserValidator $userValidator)
    {
        $this->userValidator = $userValidator;
    }

    /**
     * @param string $user
     *
     * @return User
     *
     * @throws InvalidSchemaException
     */
    public function convert(string $user): User
    {
        $this->userValidator->validate($user);
        $jsonObject = json_decode($user);
        $user = new User();
        $user->setName($jsonObject->name)
            ->setSurname($jsonObject->surname)
            ->setPhoneNumber($jsonObject->phoneNumber)
            ->setStreet($jsonObject->street)
            ->setHouseNumber($jsonObject->houseNumber)
            ->setZipCode($jsonObject->zipCode)
            ->setCity($jsonObject->city)
            ->setBankAccountOwner($jsonObject->bankAccountOwner)
            ->setIban($jsonObject->iban);

        return $user;
    }
}
