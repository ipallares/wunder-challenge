<?php

declare(strict_types=1);

namespace App\Logic\UseCase;

use App\Entity\User;
use App\Exception\PaymentCommunicationException;
use App\Exception\PaymentNotAcceptedException;
use App\Logic\Converter\UserConverter;
use App\Logic\PaymentRequester;
use App\Repository\UserRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class UserCreator
{
    private PaymentRequester $paymentRequester;
    private UserConverter $userConverter;
    private UserRepository $userRepository;

    public function __construct(
        PaymentRequester $paymentRequester,
        UserConverter $userConverter,
        UserRepository $userRepository
    ) {
        $this->paymentRequester = $paymentRequester;
        $this->userConverter = $userConverter;
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $user
     *
     * @return User
     *
     * @throws ORMException
     * @throws PaymentCommunicationException
     * @throws PaymentNotAcceptedException
     */
    public function create(string $user) : User
    {
        try {
            $userEntity = $this->userConverter->convert($user);
            $this->userRepository->save($userEntity);
            $response = $this->paymentRequester->request($userEntity);
            $this->updateUserIfSuccess($userEntity, $response);

            return $userEntity;
        } catch (
            TransportExceptionInterface |
            ClientExceptionInterface |
            RedirectionExceptionInterface |
            ServerExceptionInterface $e ) {

            throw new PaymentCommunicationException();
        }
    }

    /**
     * @param User $user
     * @param ResponseInterface $response
     *
     * @return void
     *
     * @throws ClientExceptionInterface
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws PaymentCommunicationException
     * @throws PaymentNotAcceptedException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function updateUserIfSuccess(User $user, ResponseInterface $response): void
    {
        if ( Response::HTTP_OK !== $response->getStatusCode()) {
            throw new PaymentNotAcceptedException();
        }
        $user->setPaymentDataId($this->getPaymentDataId($response));
        $this->userRepository->save($user);
    }

    /**
     * @param ResponseInterface $response
     *
     * @return string
     *
     * @throws ClientExceptionInterface
     * @throws PaymentCommunicationException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getPaymentDataId(ResponseInterface $response): string
    {
        $responseContent = json_decode($response->getContent());
        if (!isset($responseContent->paymentDataId)) {
            throw new PaymentCommunicationException();
        }

        return $responseContent->paymentDataId;
    }
}
