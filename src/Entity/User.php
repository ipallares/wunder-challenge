<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private string $surname;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private string $phoneNumber;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private string $street;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private string $houseNumber;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private string $zipCode;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private string $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $bankAccountOwner;

    /**
     * @ORM\Column(type="string", length=34)
     */
    private string $iban;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private string $paymentDataId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getBankAccountOwner(): ?string
    {
        return $this->bankAccountOwner;
    }

    public function setBankAccountOwner(string $bankAccountOwner): self
    {
        $this->bankAccountOwner = $bankAccountOwner;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getPaymentDataId(): ?string
    {
        return $this->paymentDataId;
    }

    public function setPaymentDataId(?string $paymentDataId): self
    {
        $this->paymentDataId = $paymentDataId;

        return $this;
    }
}
