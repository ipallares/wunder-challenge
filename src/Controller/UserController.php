<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\PaymentCommunicationException;
use App\Exception\PaymentNotAcceptedException;
use App\Logic\UseCase\UserCreator;
use Exception;
use JsonException;
use JsonSchema\Exception\InvalidSchemaException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{

    /**
     * @Route("/user", name="create_user", methods="POST")
     *
     * @param LoggerInterface $logger
     * @param UserCreator $userCreator
     * @param Request $request
     *
     * @return Response
     */
    public function createUserAction(
        LoggerInterface $logger,
        UserCreator $userCreator,
        Request $request
    ): Response
    {
        try {
            $userJsonInfo = $request->getContent();
            $user = $userCreator->create($userJsonInfo);

            return $this->json('{"paymentDataId": "'.$user->getPaymentDataId().'"}', 200);
        } catch(InvalidSchemaException | JsonException $e) {
            $logger->error($e->getMessage(), $e->getTrace());

            return $this->json($userJsonInfo, 422);
        } catch(PaymentNotAcceptedException $e) {
            $logger->error($e->getMessage(), $e->getTrace());

            return $this->json($userJsonInfo, 402);
        } catch(PaymentCommunicationException $e) {
            $logger->error($e->getMessage(), $e->getTrace());

            return $this->json($userJsonInfo, 502);
        } catch(Exception $e) {
            $logger->error($e->getMessage(), $e->getTrace());

            return $this->json($userJsonInfo, 500);
        }
    }
}
