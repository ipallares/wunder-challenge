<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setName('Max')
            ->setSurname('Mustermann')
            ->setPhoneNumber('555-555-555')
            ->setStreet('Musterstrasse')
            ->setHouseNumber('134B')
            ->setZipCode('10435')
            ->setCity('Berlin')
            ->setBankAccountOwner('Max Mustermann')
            ->setIban('DE89-1234-124-123-123');

        $manager->persist($user);
        $manager->flush();
    }
}
